EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Mechanical:MountingHole_Pad H1
U 1 1 5FB5EA20
P 3000 1550
F 0 "H1" H 3100 1599 50  0000 L CNN
F 1 "MountingHole_Pad" H 3100 1508 50  0000 L CNN
F 2 "Kosmo_panel:Kosmo_Pot_Hole" H 3000 1550 50  0001 C CNN
F 3 "~" H 3000 1550 50  0001 C CNN
	1    3000 1550
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H3
U 1 1 5FB5F9B9
P 3000 2050
F 0 "H3" H 3100 2099 50  0000 L CNN
F 1 "MountingHole_Pad" H 3100 2008 50  0000 L CNN
F 2 "Kosmo_panel:Kosmo_LED_Hole" H 3000 2050 50  0001 C CNN
F 3 "~" H 3000 2050 50  0001 C CNN
	1    3000 2050
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H4
U 1 1 5FB5FC55
P 3000 2550
F 0 "H4" H 3100 2599 50  0000 L CNN
F 1 "MountingHole_Pad" H 3100 2508 50  0000 L CNN
F 2 "Kosmo_panel:Kosmo_Jack_Hole" H 3000 2550 50  0001 C CNN
F 3 "~" H 3000 2550 50  0001 C CNN
	1    3000 2550
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H10
U 1 1 5FB5FFAF
P 3000 3050
F 0 "H10" H 3100 3099 50  0000 L CNN
F 1 "MountingHole_Pad" H 3100 3008 50  0000 L CNN
F 2 "Kosmo_panel:Kosmo_Panel_Slotted_Mounting_Hole" H 3000 3050 50  0001 C CNN
F 3 "~" H 3000 3050 50  0001 C CNN
	1    3000 3050
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H2
U 1 1 5FB63387
P 3500 1550
F 0 "H2" H 3600 1599 50  0000 L CNN
F 1 "MountingHole_Pad" H 3600 1508 50  0000 L CNN
F 2 "Kosmo_panel:Kosmo_Pot_Hole" H 3500 1550 50  0001 C CNN
F 3 "~" H 3500 1550 50  0001 C CNN
	1    3500 1550
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H5
U 1 1 5FB633A5
P 3500 2550
F 0 "H5" H 3600 2599 50  0000 L CNN
F 1 "MountingHole_Pad" H 3600 2508 50  0000 L CNN
F 2 "Kosmo_panel:Kosmo_Jack_Hole" H 3500 2550 50  0001 C CNN
F 3 "~" H 3500 2550 50  0001 C CNN
	1    3500 2550
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H11
U 1 1 5FB633AF
P 3500 3050
F 0 "H11" H 3600 3099 50  0000 L CNN
F 1 "MountingHole_Pad" H 3600 3008 50  0000 L CNN
F 2 "Kosmo_panel:Kosmo_Panel_Slotted_Mounting_Hole" H 3500 3050 50  0001 C CNN
F 3 "~" H 3500 3050 50  0001 C CNN
	1    3500 3050
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H6
U 1 1 5FB6451F
P 4000 2550
F 0 "H6" H 4100 2599 50  0000 L CNN
F 1 "MountingHole_Pad" H 4100 2508 50  0000 L CNN
F 2 "Kosmo_panel:Kosmo_Jack_Hole" H 4000 2550 50  0001 C CNN
F 3 "~" H 4000 2550 50  0001 C CNN
	1    4000 2550
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H12
U 1 1 5FB64533
P 4000 3050
F 0 "H12" H 4100 3099 50  0000 L CNN
F 1 "MountingHole_Pad" H 4100 3008 50  0000 L CNN
F 2 "Kosmo_panel:Kosmo_Panel_Slotted_Mounting_Hole" H 4000 3050 50  0001 C CNN
F 3 "~" H 4000 3050 50  0001 C CNN
	1    4000 3050
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H7
U 1 1 5FB6453D
P 4500 2550
F 0 "H7" H 4600 2599 50  0000 L CNN
F 1 "MountingHole_Pad" H 4600 2508 50  0000 L CNN
F 2 "Kosmo_panel:Kosmo_Jack_Hole" H 4500 2550 50  0001 C CNN
F 3 "~" H 4500 2550 50  0001 C CNN
	1    4500 2550
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H13
U 1 1 5FB64547
P 4500 3050
F 0 "H13" H 4600 3099 50  0000 L CNN
F 1 "MountingHole_Pad" H 4600 3008 50  0000 L CNN
F 2 "Kosmo_panel:Kosmo_Panel_Slotted_Mounting_Hole" H 4500 3050 50  0001 C CNN
F 3 "~" H 4500 3050 50  0001 C CNN
	1    4500 3050
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H8
U 1 1 5FB6574B
P 5000 2550
F 0 "H8" H 5100 2599 50  0000 L CNN
F 1 "MountingHole_Pad" H 5100 2508 50  0000 L CNN
F 2 "Kosmo_panel:Kosmo_Jack_Hole" H 5000 2550 50  0001 C CNN
F 3 "~" H 5000 2550 50  0001 C CNN
	1    5000 2550
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H9
U 1 1 5FB6575F
P 5500 2550
F 0 "H9" H 5600 2599 50  0000 L CNN
F 1 "MountingHole_Pad" H 5600 2508 50  0000 L CNN
F 2 "Kosmo_panel:Kosmo_Jack_Hole" H 5500 2550 50  0001 C CNN
F 3 "~" H 5500 2550 50  0001 C CNN
	1    5500 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	3500 1650 3000 1650
Wire Wire Line
	2750 3150 3000 3150
Connection ~ 3000 1650
Wire Wire Line
	3000 1650 2750 1650
Connection ~ 3000 3150
Wire Wire Line
	3000 3150 3500 3150
Connection ~ 3500 3150
Wire Wire Line
	3500 3150 4000 3150
Connection ~ 4000 3150
Wire Wire Line
	4000 3150 4500 3150
Wire Wire Line
	5500 2650 5000 2650
Wire Wire Line
	2750 1650 2750 2150
Connection ~ 2750 2650
Wire Wire Line
	2750 2650 2750 3150
Connection ~ 3000 2650
Wire Wire Line
	3000 2650 2750 2650
Connection ~ 3500 2650
Wire Wire Line
	3500 2650 3000 2650
Connection ~ 4000 2650
Wire Wire Line
	4000 2650 3500 2650
Connection ~ 4500 2650
Wire Wire Line
	4500 2650 4000 2650
Connection ~ 5000 2650
Wire Wire Line
	5000 2650 4500 2650
Wire Wire Line
	3000 2150 2750 2150
Connection ~ 2750 2150
Wire Wire Line
	2750 2150 2750 2650
$Comp
L power:GND #PWR?
U 1 1 5FB66CE3
P 2750 3150
F 0 "#PWR?" H 2750 2900 50  0001 C CNN
F 1 "GND" H 2755 2977 50  0000 C CNN
F 2 "" H 2750 3150 50  0001 C CNN
F 3 "" H 2750 3150 50  0001 C CNN
	1    2750 3150
	1    0    0    -1  
$EndComp
Connection ~ 2750 3150
$EndSCHEMATC
