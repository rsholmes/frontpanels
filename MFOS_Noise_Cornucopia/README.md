Kosmo format front panel for the Music From Outer Space Noise Cornucopia synth module.

Build the MFOS circuit board unmodified, mount using a bracket attached with pots.

![](Images/IMG_6509.JPG)
