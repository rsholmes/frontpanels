# Kassutronics Wavefolder

This is a panel and PCB clamp for a Kosmo version of the Kassutronics Wavefolder. It includes an extra jack and pot for a Symmetry (Odd/Even) CV and another extra jack for a second VCA CV, as described near the end of the Kassutronics build document.

The PCB is built according to the Kassutronics instructions except the board mounted jacks and pots are omitted.

(Also, the -9 V regulator (LM7909) is rare and hard to find. So I substituted ±8 V regulators (LM7808 and LM7908) which work fine.)

The second VCA CV jack tip and sleeve are wired to footprint J6 via a 10k resistor in series with the tip.

The Symmetry CV pot is a B100k wired as follows: Counterclockwise pin to Symmetry CV jack tip. Wiper pin to footprint J8 via a 1M series resistor. Clockwise pin to footprint J8 and to Symmetry CV jack sleeve.

The remaining jacks and pots are wired to the corresponding jack and pot footprints on the PCB.

To attach the PCB to the panel I used a 3D printed clamp; STL files are in this repo. Use an M4 screw to secure the clamp to the PCB. The foot of the clamp goes between the VCA CV pot and the panel.

## Photos

![front](Images/front.jpg)

![front](Images/back.jpg)

## Git repository

* [https://gitlab.com/rsholmes/](https://gitlab.com/rsholmes/frontpanels/Kassutronics_Wavefolder)

