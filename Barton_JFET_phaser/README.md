# Barton JFET Phaser

This is a panel for a Kosmo version of the [Barton JFET Phaser](https://bartonmusicalcircuits.com/phaser/index.html). 

The PCB is built according to the Barton instructions except the board mounted jacks and pots are omitted (and, as I usually do with Barton designs, I used 100 nF for the bypass caps and 1N5817 Schottky diodes instead of 10Ω resistors on the power rails). Panel mounted jacks and pots are wired to the corresponding jack and pot footprints on the PCB.

To attach the PCB to the panel I used a sheet aluminum bracket, clamped between the bottom two pots and the panel.

## Photos

![front](Images/front.jpg)

![front](Images/back.jpg)

## Git repository

* [https://gitlab.com/rsholmes/frontpanels/-/tree/main/Barton_JFET_phaser](https://gitlab.com/rsholmes/frontpanels/-/tree/main/Barton_JFET_phaser)

