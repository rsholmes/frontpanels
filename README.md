# Front panels

Here are KiCad design files and Gerber files for front panels for various synth modules. Upload Gerbers to a PCB fabricator to have them made for you.

Panels included:

* Kosmo format (20 cm high)
    * Attenuators (Analog Output)
    * Comparators (YuSynth)
    * Dual Rectifiers (Barton Musical Circuits)
    * Dual VCA (Music From Outer Space)
    * Sallen-Key VCF (Barton Musical Circuits)
    * Neuron/Difference Rectifier (Nonlinear Circuits)
    * Noise Cornucopia (Music From Outer Space)
    * VCO (Music From Outer Space)
  
## Git repository

* [https://gitlab.com/rsholmes/frontpanels](https://gitlab.com/rsholmes/frontpanels)


