Kosmo format front panel for the Nonlinear Circuits Neuron/Difference Rectifier synth module.

Build the NLC circuit board unmodified, but omitting the jacks and pots, and you can omit the 100k resistors associated with the first input to the Neuron and the first + and - inputs to the Difference Rectifier. Wire panel mounted jacks and pots to the second and third input jacks, output jacks, and pot footprints on the PCB:

![](Images/wiring.png)

![](Images/back.jpg)

Use 35 mm standoffs to secure PCB to panel, using holes in panel and two of the unused jack mounting holes on the PCB.

![](Images/side.jpg)

![](Images/front.jpg)
