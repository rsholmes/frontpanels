EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Mechanical:MountingHole_Pad H1
U 1 1 5F6F2543
P 2250 1850
F 0 "H1" H 2350 1899 50  0000 L CNN
F 1 "MountingHole_Pad" H 2350 1808 50  0000 L CNN
F 2 "Kosmo_panel:Kosmo_Pot_Hole" H 2250 1850 50  0001 C CNN
F 3 "~" H 2250 1850 50  0001 C CNN
	1    2250 1850
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H2
U 1 1 5F6F2D63
P 2250 2350
F 0 "H2" H 2350 2399 50  0000 L CNN
F 1 "MountingHole_Pad" H 2350 2308 50  0000 L CNN
F 2 "Kosmo_panel:Kosmo_Pot_Hole" H 2250 2350 50  0001 C CNN
F 3 "~" H 2250 2350 50  0001 C CNN
	1    2250 2350
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H3
U 1 1 5F6F3078
P 2250 2850
F 0 "H3" H 2350 2899 50  0000 L CNN
F 1 "MountingHole_Pad" H 2350 2808 50  0000 L CNN
F 2 "Kosmo_panel:Kosmo_Pot_Hole" H 2250 2850 50  0001 C CNN
F 3 "~" H 2250 2850 50  0001 C CNN
	1    2250 2850
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H4
U 1 1 5F6F349D
P 3500 1850
F 0 "H4" H 3600 1899 50  0000 L CNN
F 1 "MountingHole_Pad" H 3600 1808 50  0000 L CNN
F 2 "Kosmo_panel:Kosmo_Jack_Hole" H 3500 1850 50  0001 C CNN
F 3 "~" H 3500 1850 50  0001 C CNN
	1    3500 1850
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H5
U 1 1 5F6F37D5
P 3500 2350
F 0 "H5" H 3600 2399 50  0000 L CNN
F 1 "MountingHole_Pad" H 3600 2308 50  0000 L CNN
F 2 "Kosmo_panel:Kosmo_Jack_Hole" H 3500 2350 50  0001 C CNN
F 3 "~" H 3500 2350 50  0001 C CNN
	1    3500 2350
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H6
U 1 1 5F6F3BCF
P 3500 2850
F 0 "H6" H 3600 2899 50  0000 L CNN
F 1 "MountingHole_Pad" H 3600 2808 50  0000 L CNN
F 2 "Kosmo_panel:Kosmo_Jack_Hole" H 3500 2850 50  0001 C CNN
F 3 "~" H 3500 2850 50  0001 C CNN
	1    3500 2850
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H7
U 1 1 5F6F4F3A
P 4750 1850
F 0 "H7" H 4850 1899 50  0000 L CNN
F 1 "MountingHole_Pad" H 4850 1808 50  0000 L CNN
F 2 "Kosmo_panel:Kosmo_Jack_Hole" H 4750 1850 50  0001 C CNN
F 3 "~" H 4750 1850 50  0001 C CNN
	1    4750 1850
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H8
U 1 1 5F6F544F
P 4750 2350
F 0 "H8" H 4850 2399 50  0000 L CNN
F 1 "MountingHole_Pad" H 4850 2308 50  0000 L CNN
F 2 "Kosmo_panel:Kosmo_Panel_Slotted_Mounting_Hole" H 4750 2350 50  0001 C CNN
F 3 "~" H 4750 2350 50  0001 C CNN
	1    4750 2350
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H9
U 1 1 5F6F5756
P 4750 2850
F 0 "H9" H 4850 2899 50  0000 L CNN
F 1 "MountingHole_Pad" H 4850 2808 50  0000 L CNN
F 2 "Kosmo_panel:Kosmo_Panel_Slotted_Mounting_Hole" H 4750 2850 50  0001 C CNN
F 3 "~" H 4750 2850 50  0001 C CNN
	1    4750 2850
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H10
U 1 1 5F6F5B20
P 6000 1850
F 0 "H10" H 6100 1899 50  0000 L CNN
F 1 "MountingHole_Pad" H 6100 1808 50  0000 L CNN
F 2 "Kosmo_panel:Kosmo_Panel_Slotted_Mounting_Hole" H 6000 1850 50  0001 C CNN
F 3 "~" H 6000 1850 50  0001 C CNN
	1    6000 1850
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H11
U 1 1 5F6F5ED6
P 6000 2350
F 0 "H11" H 6100 2399 50  0000 L CNN
F 1 "MountingHole_Pad" H 6100 2308 50  0000 L CNN
F 2 "Kosmo_panel:Kosmo_Panel_Slotted_Mounting_Hole" H 6000 2350 50  0001 C CNN
F 3 "~" H 6000 2350 50  0001 C CNN
	1    6000 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	2250 1950 3500 1950
Wire Wire Line
	6400 1950 6400 2450
Wire Wire Line
	6400 2450 6000 2450
Connection ~ 3500 1950
Wire Wire Line
	3500 1950 4750 1950
Connection ~ 4750 1950
Wire Wire Line
	4750 1950 6000 1950
Connection ~ 6000 1950
Wire Wire Line
	6000 1950 6400 1950
Connection ~ 3500 2450
Wire Wire Line
	3500 2450 2250 2450
Connection ~ 4750 2450
Wire Wire Line
	4750 2450 3500 2450
Connection ~ 6000 2450
Wire Wire Line
	6000 2450 4750 2450
Wire Wire Line
	2250 2450 1800 2450
Wire Wire Line
	1800 2450 1800 2950
Wire Wire Line
	1800 2950 2250 2950
Connection ~ 2250 2450
Connection ~ 2250 2950
Wire Wire Line
	2250 2950 3500 2950
Connection ~ 3500 2950
Wire Wire Line
	3500 2950 4400 2950
$Comp
L power:GND #PWR0101
U 1 1 5F6F6641
P 4400 2950
F 0 "#PWR0101" H 4400 2700 50  0001 C CNN
F 1 "GND" H 4405 2777 50  0000 C CNN
F 2 "" H 4400 2950 50  0001 C CNN
F 3 "" H 4400 2950 50  0001 C CNN
	1    4400 2950
	1    0    0    -1  
$EndComp
Connection ~ 4400 2950
Wire Wire Line
	4400 2950 4750 2950
$EndSCHEMATC
