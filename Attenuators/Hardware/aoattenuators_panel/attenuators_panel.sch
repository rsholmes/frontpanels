EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Mechanical:MountingHole_Pad H1
U 1 1 5F8CC1CE
P 3000 2550
F 0 "H1" H 3100 2599 50  0000 L CNN
F 1 "MountingHole_Pad" H 3100 2508 50  0000 L CNN
F 2 "Kosmo_panel:Kosmo_Panel_Slotted_Mounting_Hole" H 3000 2550 50  0001 C CNN
F 3 "~" H 3000 2550 50  0001 C CNN
	1    3000 2550
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H5
U 1 1 5F8CC5F9
P 3500 2550
F 0 "H5" H 3600 2599 50  0000 L CNN
F 1 "MountingHole_Pad" H 3600 2508 50  0000 L CNN
F 2 "Kosmo_panel:Kosmo_Pot_Hole" H 3500 2550 50  0001 C CNN
F 3 "~" H 3500 2550 50  0001 C CNN
	1    3500 2550
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H9
U 1 1 5F8CC8BB
P 4000 2550
F 0 "H9" H 4100 2599 50  0000 L CNN
F 1 "MountingHole_Pad" H 4100 2508 50  0000 L CNN
F 2 "Kosmo_panel:Kosmo_Jack_Hole" H 4000 2550 50  0001 C CNN
F 3 "~" H 4000 2550 50  0001 C CNN
	1    4000 2550
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H13
U 1 1 5F8CCE73
P 4500 2550
F 0 "H13" H 4600 2599 50  0000 L CNN
F 1 "MountingHole_Pad" H 4600 2508 50  0000 L CNN
F 2 "Kosmo_panel:Kosmo_Jack_Hole" H 4500 2550 50  0001 C CNN
F 3 "~" H 4500 2550 50  0001 C CNN
	1    4500 2550
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H2
U 1 1 5F8CDD2D
P 3000 3050
F 0 "H2" H 3100 3099 50  0000 L CNN
F 1 "MountingHole_Pad" H 3100 3008 50  0000 L CNN
F 2 "Kosmo_panel:Kosmo_Panel_Slotted_Mounting_Hole" H 3000 3050 50  0001 C CNN
F 3 "~" H 3000 3050 50  0001 C CNN
	1    3000 3050
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H6
U 1 1 5F8CDD33
P 3500 3050
F 0 "H6" H 3600 3099 50  0000 L CNN
F 1 "MountingHole_Pad" H 3600 3008 50  0000 L CNN
F 2 "Kosmo_panel:Kosmo_Pot_Hole" H 3500 3050 50  0001 C CNN
F 3 "~" H 3500 3050 50  0001 C CNN
	1    3500 3050
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H10
U 1 1 5F8CDD39
P 4000 3050
F 0 "H10" H 4100 3099 50  0000 L CNN
F 1 "MountingHole_Pad" H 4100 3008 50  0000 L CNN
F 2 "Kosmo_panel:Kosmo_Jack_Hole" H 4000 3050 50  0001 C CNN
F 3 "~" H 4000 3050 50  0001 C CNN
	1    4000 3050
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H3
U 1 1 5F8CE86A
P 3000 3550
F 0 "H3" H 3100 3599 50  0000 L CNN
F 1 "MountingHole_Pad" H 3100 3508 50  0000 L CNN
F 2 "Kosmo_panel:Kosmo_Panel_Slotted_Mounting_Hole" H 3000 3550 50  0001 C CNN
F 3 "~" H 3000 3550 50  0001 C CNN
	1    3000 3550
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H7
U 1 1 5F8CE870
P 3500 3550
F 0 "H7" H 3600 3599 50  0000 L CNN
F 1 "MountingHole_Pad" H 3600 3508 50  0000 L CNN
F 2 "Kosmo_panel:Kosmo_Pot_Hole" H 3500 3550 50  0001 C CNN
F 3 "~" H 3500 3550 50  0001 C CNN
	1    3500 3550
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H11
U 1 1 5F8CE876
P 4000 3550
F 0 "H11" H 4100 3599 50  0000 L CNN
F 1 "MountingHole_Pad" H 4100 3508 50  0000 L CNN
F 2 "Kosmo_panel:Kosmo_Jack_Hole" H 4000 3550 50  0001 C CNN
F 3 "~" H 4000 3550 50  0001 C CNN
	1    4000 3550
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H4
U 1 1 5F8CF3F3
P 3000 4050
F 0 "H4" H 3100 4099 50  0000 L CNN
F 1 "MountingHole_Pad" H 3100 4008 50  0000 L CNN
F 2 "Kosmo_panel:Kosmo_Panel_Slotted_Mounting_Hole" H 3000 4050 50  0001 C CNN
F 3 "~" H 3000 4050 50  0001 C CNN
	1    3000 4050
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H8
U 1 1 5F8CF3F9
P 3500 4050
F 0 "H8" H 3600 4099 50  0000 L CNN
F 1 "MountingHole_Pad" H 3600 4008 50  0000 L CNN
F 2 "Kosmo_panel:Kosmo_Jack_Hole" H 3500 4050 50  0001 C CNN
F 3 "~" H 3500 4050 50  0001 C CNN
	1    3500 4050
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H12
U 1 1 5F8CF3FF
P 4000 4050
F 0 "H12" H 4100 4099 50  0000 L CNN
F 1 "MountingHole_Pad" H 4100 4008 50  0000 L CNN
F 2 "Kosmo_panel:Kosmo_Jack_Hole" H 4000 4050 50  0001 C CNN
F 3 "~" H 4000 4050 50  0001 C CNN
	1    4000 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 2650 3500 2650
Connection ~ 3500 2650
Wire Wire Line
	3500 2650 4000 2650
Connection ~ 4000 2650
Wire Wire Line
	4000 2650 4500 2650
Wire Wire Line
	4500 2650 4500 3150
Wire Wire Line
	4500 3150 4000 3150
Wire Wire Line
	2850 3150 2850 3650
Wire Wire Line
	2850 3650 3000 3650
Wire Wire Line
	4200 3650 4200 4150
Wire Wire Line
	4200 4150 4000 4150
Connection ~ 4500 2650
Connection ~ 3000 3150
Wire Wire Line
	3000 3150 2850 3150
Connection ~ 3500 3150
Wire Wire Line
	3500 3150 3000 3150
Connection ~ 4000 3150
Wire Wire Line
	4000 3150 3500 3150
Connection ~ 3000 3650
Wire Wire Line
	3000 3650 3500 3650
Connection ~ 3500 3650
Wire Wire Line
	3500 3650 4000 3650
Connection ~ 4000 3650
Wire Wire Line
	4000 3650 4200 3650
Connection ~ 3000 4150
Wire Wire Line
	3000 4150 2850 4150
Connection ~ 3500 4150
Wire Wire Line
	3500 4150 3000 4150
Connection ~ 4000 4150
Wire Wire Line
	4000 4150 3500 4150
$Comp
L power:GND #PWR01
U 1 1 5F8CFF1F
P 2850 4150
F 0 "#PWR01" H 2850 3900 50  0001 C CNN
F 1 "GND" H 2855 3977 50  0000 C CNN
F 2 "" H 2850 4150 50  0001 C CNN
F 3 "" H 2850 4150 50  0001 C CNN
	1    2850 4150
	1    0    0    -1  
$EndComp
$EndSCHEMATC
