# Circuit changes

The following changes from the Analog Drum / Add Noise Drum probably should be carried over:

## Variable threshold

Add sensitivity knob. Requires some kludging on PCB — I think just replacing the 100k and 1k resistors with 20k and 100R soldered at one end only, other ends to 10k pot terminals, pot wiper to unused pad of one resistor. (Or adjust values proportionally.)

## Envelope generators

There is only one so it probably could be left alone, though on the other hand it couldn't hurt to change the 220R resistor to 2.2k.

## VCO changes

It looks like the VCO is the same as in the Analog Drum (plus the square wave comparator), so the same changes should be made: Increase the 470R resistor from LM13700 pin 13 to ground to 1k and the capacitor from 22 nF to 47 nF. It looks like the comparator is set up to give about ±5.4 V which is good enough.

## VCA changes

This also looks just like the Analog Drum VCA and should be changed similarly. Increase the signal input resistor from 22k to 39k and the 10k current source resistor to 20k.

## Other changes

As usual with Barton, change 10R resistors on the voltage rails to Schottkys, and 10 nF bypass capacitors to 100 nF.

