# Comparators build notes

## ±12 V

Usson designed this module for ±15 V power. It would work with ±12 V power but it's better with some changes. To keep the comparator A threshold limits at ±8 V I changed the pot to 100k and the fixed resistors to 22k. I did not change the pot or fixed resistor for the comparator B threshold because, while Usson says it varies from 0 to 8 V, it really goes higher with a ±15 V supply; with ±12 V power, it really does go to 8 V.

If your potentiometers measure out at much above or below 100k, you might want to adjust those resistors to get the intended threshold range, though really it doesn't matter much.

The output jacks all have voltage dividers to reduce the levels to near 5 V. Usson used 1.2k over 1k. For ±12 V power, I chose to use 1k over 1.2k — same resistor values, but switched around.

Additionally, I changed the resistors in the voltage dividers for the trigger comparator thresholds from 12k to 10k. This has nothing to do with ±12 V power, I just have more 10k resistors than 12k, and it is only the 1:1 ratio that matters.

## Bypass capacitors

Usson's design uses a single 100 nF capacitor per op amp chip, from one power rail to the other. What I have read is that this is poor practice, and there should be two capacitors per chip, one from each power rail to ground. This probably doesn't really matter for audio circuits like these, but I nevertheless kludged two caps per chip by putting one lead of each into the provided footprint, connecting the other two leads together, and connecting them to ground. Because I'm like that.

## PCB modifications

The changes I made to the resistors and bypass caps are shown below:

![comparators_changes](../Images/comparators_changes.png)

## Power header

The PCB (as received from Soundtronics) has a so called Eurorack power header footprint which is wrong. The pin spacing lengthwise is correct but too wide across the width. A standard 2.54 mm pitch header will not fit.

I got around this by using a small piece of stripboard, soldered to the PCB via a 1x5 pin header, with the power header mounted to the stripboard.

![power_header](../Images/power_header.jpg)

## LEDs

In Usson's circuit, the input signal connects to an inverting comparator, whose output is near -15 V (with ±15 V power) if the signal is above threshold and near +15 V if it is below threshold. This goes to the gate and trigger op amps, but also via a 1.5k resistor to the cathode of an LED whose anode connects to ground. When the signal is above threshold, the LED conducts and lights up. When the signal is below threshold... the LED is reverse biased at 15 V. But most LEDs have an absolute maximum reverse voltage of something like 5 V. In the Kosmo version the reverse bias is only 12 V, but that is still way too high.

I addressed this by soldering a 1N4148 diode across the LED legs, cathode to anode and anode to cathode so the two diodes are antiparallel. Now when the signal is below threshold, the 1N4148 conducts and the LED is only reverse biased by about 700 mV.

![LED](../Images/LED.png)

## One more thing

I may be delusional, but I think the R5 and R6 references on the PCB are reversed.
