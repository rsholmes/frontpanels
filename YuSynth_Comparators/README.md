This is a front panel and suggested mods for a Kosmo format version of Yves Usson's [Comparators](http://yusynth.net/Modular/EN/COMPARATORS/index.html) module from YuSynth.


The modifications do not add features but adapt the design for ±12 V and correct or work around a couple of issues. See [Build notes](Docs/build.md)


## Current draw
14.1 mA +12 V, 8.9 mA -12 V


## Photos

![front](Images/front.jpg)
![front](Images/back.jpg)

## Documentation

* [Build notes](Docs/build.md)
* [Blog post](https://analogoutputblog.wordpress.com/2022/05/18/yusynth-comparators/)

## Git repository

* [https://gitlab.com/rsholmes/frontpanels/tree/main/YuSynth_Comparators](https://gitlab.com/rsholmes/frontpanels/tree/main/YuSynth_Comparators)

