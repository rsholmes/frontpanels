EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Mechanical:MountingHole_Pad H1
U 1 1 5FB200A1
P 2500 2050
F 0 "H1" H 2600 2099 50  0000 L CNN
F 1 "MountingHole_Pad" H 2600 2008 50  0000 L CNN
F 2 "Kosmo_panel:Kosmo_Pot_Hole" H 2500 2050 50  0001 C CNN
F 3 "~" H 2500 2050 50  0001 C CNN
	1    2500 2050
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H3
U 1 1 5FB21886
P 2500 3050
F 0 "H3" H 2600 3099 50  0000 L CNN
F 1 "MountingHole_Pad" H 2600 3008 50  0000 L CNN
F 2 "Kosmo_panel:Kosmo_Jack_Hole" H 2500 3050 50  0001 C CNN
F 3 "~" H 2500 3050 50  0001 C CNN
	1    2500 3050
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H2
U 1 1 5FB262BA
P 2500 2550
F 0 "H2" H 2600 2599 50  0000 L CNN
F 1 "MountingHole_Pad" H 2600 2508 50  0000 L CNN
F 2 "Kosmo_panel:Kosmo_Switch_Hole" H 2500 2550 50  0001 C CNN
F 3 "~" H 2500 2550 50  0001 C CNN
	1    2500 2550
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H5
U 1 1 5FB26EB0
P 2500 4050
F 0 "H5" H 2600 4099 50  0000 L CNN
F 1 "MountingHole_Pad" H 2600 4008 50  0000 L CNN
F 2 "Kosmo_panel:Kosmo_Panel_Slotted_Mounting_Hole" H 2500 4050 50  0001 C CNN
F 3 "~" H 2500 4050 50  0001 C CNN
	1    2500 4050
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H4
U 1 1 5FB27330
P 2500 3550
F 0 "H4" H 2600 3599 50  0000 L CNN
F 1 "MountingHole_Pad" H 2600 3508 50  0000 L CNN
F 2 "Kosmo_panel:Kosmo_Jack_Hole" H 2500 3550 50  0001 C CNN
F 3 "~" H 2500 3550 50  0001 C CNN
	1    2500 3550
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H6
U 1 1 5FB28336
P 3000 2050
F 0 "H6" H 3100 2099 50  0000 L CNN
F 1 "MountingHole_Pad" H 3100 2008 50  0000 L CNN
F 2 "Kosmo_panel:Kosmo_Pot_Hole" H 3000 2050 50  0001 C CNN
F 3 "~" H 3000 2050 50  0001 C CNN
	1    3000 2050
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H7
U 1 1 5FB2834A
P 3000 3050
F 0 "H7" H 3100 3099 50  0000 L CNN
F 1 "MountingHole_Pad" H 3100 3008 50  0000 L CNN
F 2 "Kosmo_panel:Kosmo_Jack_Hole" H 3000 3050 50  0001 C CNN
F 3 "~" H 3000 3050 50  0001 C CNN
	1    3000 3050
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H9
U 1 1 5FB2835E
P 3000 4050
F 0 "H9" H 3100 4099 50  0000 L CNN
F 1 "MountingHole_Pad" H 3100 4008 50  0000 L CNN
F 2 "Kosmo_panel:Kosmo_Panel_Slotted_Mounting_Hole" H 3000 4050 50  0001 C CNN
F 3 "~" H 3000 4050 50  0001 C CNN
	1    3000 4050
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H8
U 1 1 5FB28368
P 3000 3550
F 0 "H8" H 3100 3599 50  0000 L CNN
F 1 "MountingHole_Pad" H 3100 3508 50  0000 L CNN
F 2 "Kosmo_panel:Kosmo_Jack_Hole" H 3000 3550 50  0001 C CNN
F 3 "~" H 3000 3550 50  0001 C CNN
	1    3000 3550
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H10
U 1 1 5FB2920D
P 3500 2050
F 0 "H10" H 3600 2099 50  0000 L CNN
F 1 "MountingHole_Pad" H 3600 2008 50  0000 L CNN
F 2 "Kosmo_panel:Kosmo_Pot_Hole" H 3500 2050 50  0001 C CNN
F 3 "~" H 3500 2050 50  0001 C CNN
	1    3500 2050
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H11
U 1 1 5FB29221
P 3500 3050
F 0 "H11" H 3600 3099 50  0000 L CNN
F 1 "MountingHole_Pad" H 3600 3008 50  0000 L CNN
F 2 "Kosmo_panel:Kosmo_Jack_Hole" H 3500 3050 50  0001 C CNN
F 3 "~" H 3500 3050 50  0001 C CNN
	1    3500 3050
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H13
U 1 1 5FB2922B
P 3500 4050
F 0 "H13" H 3600 4099 50  0000 L CNN
F 1 "MountingHole_Pad" H 3600 4008 50  0000 L CNN
F 2 "Kosmo_panel:Kosmo_Panel_Slotted_Mounting_Hole" H 3500 4050 50  0001 C CNN
F 3 "~" H 3500 4050 50  0001 C CNN
	1    3500 4050
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H12
U 1 1 5FB29235
P 3500 3550
F 0 "H12" H 3600 3599 50  0000 L CNN
F 1 "MountingHole_Pad" H 3600 3508 50  0000 L CNN
F 2 "Kosmo_panel:Kosmo_Jack_Hole" H 3500 3550 50  0001 C CNN
F 3 "~" H 3500 3550 50  0001 C CNN
	1    3500 3550
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H14
U 1 1 5FB29C77
P 4000 2050
F 0 "H14" H 4100 2099 50  0000 L CNN
F 1 "MountingHole_Pad" H 4100 2008 50  0000 L CNN
F 2 "Kosmo_panel:Kosmo_Pot_Hole" H 4000 2050 50  0001 C CNN
F 3 "~" H 4000 2050 50  0001 C CNN
	1    4000 2050
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H15
U 1 1 5FB29C8B
P 4000 3050
F 0 "H15" H 4100 3099 50  0000 L CNN
F 1 "MountingHole_Pad" H 4100 3008 50  0000 L CNN
F 2 "Kosmo_panel:Kosmo_Jack_Hole" H 4000 3050 50  0001 C CNN
F 3 "~" H 4000 3050 50  0001 C CNN
	1    4000 3050
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H17
U 1 1 5FB29C95
P 4000 4050
F 0 "H17" H 4100 4099 50  0000 L CNN
F 1 "MountingHole_Pad" H 4100 4008 50  0000 L CNN
F 2 "Kosmo_panel:Kosmo_Panel_Slotted_Mounting_Hole" H 4000 4050 50  0001 C CNN
F 3 "~" H 4000 4050 50  0001 C CNN
	1    4000 4050
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H16
U 1 1 5FB29C9F
P 4000 3550
F 0 "H16" H 4100 3599 50  0000 L CNN
F 1 "MountingHole_Pad" H 4100 3508 50  0000 L CNN
F 2 "Kosmo_panel:Kosmo_Jack_Hole" H 4000 3550 50  0001 C CNN
F 3 "~" H 4000 3550 50  0001 C CNN
	1    4000 3550
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H18
U 1 1 5FB2A61B
P 4500 2050
F 0 "H18" H 4600 2099 50  0000 L CNN
F 1 "MountingHole_Pad" H 4600 2008 50  0000 L CNN
F 2 "Kosmo_panel:Kosmo_Pot_Hole" H 4500 2050 50  0001 C CNN
F 3 "~" H 4500 2050 50  0001 C CNN
	1    4500 2050
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H19
U 1 1 5FB2A62F
P 4500 3050
F 0 "H19" H 4600 3099 50  0000 L CNN
F 1 "MountingHole_Pad" H 4600 3008 50  0000 L CNN
F 2 "Kosmo_panel:Kosmo_Jack_Hole" H 4500 3050 50  0001 C CNN
F 3 "~" H 4500 3050 50  0001 C CNN
	1    4500 3050
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H20
U 1 1 5FB2BB4C
P 5000 2050
F 0 "H20" H 5100 2099 50  0000 L CNN
F 1 "MountingHole_Pad" H 5100 2008 50  0000 L CNN
F 2 "Kosmo_panel:Kosmo_Pot_Hole" H 5000 2050 50  0001 C CNN
F 3 "~" H 5000 2050 50  0001 C CNN
	1    5000 2050
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H21
U 1 1 5FB2BB60
P 5000 3050
F 0 "H21" H 5100 3099 50  0000 L CNN
F 1 "MountingHole_Pad" H 5100 3008 50  0000 L CNN
F 2 "Kosmo_panel:Kosmo_Jack_Hole" H 5000 3050 50  0001 C CNN
F 3 "~" H 5000 3050 50  0001 C CNN
	1    5000 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 2150 4500 2150
Wire Wire Line
	2250 2150 2250 2650
Wire Wire Line
	2250 2650 2500 2650
Connection ~ 2500 2150
Wire Wire Line
	2500 2150 2250 2150
Connection ~ 3000 2150
Wire Wire Line
	3000 2150 2500 2150
Connection ~ 3500 2150
Wire Wire Line
	3500 2150 3000 2150
Connection ~ 4000 2150
Wire Wire Line
	4000 2150 3500 2150
Connection ~ 4500 2150
Wire Wire Line
	4500 2150 4000 2150
Wire Wire Line
	2250 2650 2250 3150
Wire Wire Line
	2250 3150 2500 3150
Connection ~ 2250 2650
Connection ~ 2500 3150
Wire Wire Line
	2500 3150 3000 3150
Connection ~ 3000 3150
Wire Wire Line
	3000 3150 3500 3150
Connection ~ 3500 3150
Wire Wire Line
	3500 3150 4000 3150
Connection ~ 4000 3150
Wire Wire Line
	4000 3150 4500 3150
Connection ~ 4500 3150
Wire Wire Line
	4500 3150 5000 3150
Wire Wire Line
	2250 3150 2250 3650
Wire Wire Line
	2250 3650 2500 3650
Connection ~ 2250 3150
Connection ~ 2500 3650
Wire Wire Line
	2500 3650 3000 3650
Connection ~ 3000 3650
Wire Wire Line
	3000 3650 3500 3650
Connection ~ 3500 3650
Wire Wire Line
	3500 3650 4000 3650
Wire Wire Line
	2250 3650 2250 4150
Wire Wire Line
	2250 4150 2500 4150
Connection ~ 2250 3650
Connection ~ 2500 4150
Wire Wire Line
	2500 4150 3000 4150
Connection ~ 3000 4150
Wire Wire Line
	3000 4150 3500 4150
Connection ~ 3500 4150
Wire Wire Line
	3500 4150 4000 4150
$Comp
L power:GND #PWR0101
U 1 1 5FB2CEE3
P 2250 4150
F 0 "#PWR0101" H 2250 3900 50  0001 C CNN
F 1 "GND" H 2255 3977 50  0000 C CNN
F 2 "" H 2250 4150 50  0001 C CNN
F 3 "" H 2250 4150 50  0001 C CNN
	1    2250 4150
	1    0    0    -1  
$EndComp
Connection ~ 2250 4150
$EndSCHEMATC
