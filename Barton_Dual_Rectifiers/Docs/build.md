# Full Wave Dual Rectifier build notes

I built this using the Barton circuit board, but with some component values changed:

![fwdr_mods](/home/rsholmes/Documents/Hobbies/Music/Instruments/Synths/Barton/Full wave dual rectifier/Kosmo version/Images/fwdr_mods.png)

The 1 µF caps had a larger footprint then the 100 nF ones they replaced, so the one between the two ICs had to be mounted above the PCB, on top of the 100 nF one, with long leads extending to the PCB. Otherwise both caps would have fit between the IC sockets, but the ICs overhang the sockets a bit and there would not have been enough room for both caps between the ICs.

Since the board mounted pots were too close together for my taste in Kosmo modules, I panel mounted the two pots for the first rectifier (the topmost pot and the third one down on the PCB) and wired them to the board mounted pot footprints. The other two pots were board mounted. Jacks were wired as in Barton's design, including a connection between the first rectifier's output and the second's input tip switch.

