This is the Barton Musical Circuits BMC021 Full Wave Dual Rectifier in Kosmo format, with some modifications.

I built it using the BMC circuit board mounted behind a Kosmo format fabricated FR4 front panel.

I did make some changes. As usual with Barton modules, I used 100 nF instead of 10 nF for the bypass caps, and I replaced the 10R series resistors on the power rails with 1N5817 Schottkys for reverse voltage protection. I also changed a lot of the resistors. The input and feedback resistors on the input op amp stages need to be 100k, for proper input impedance, but the following stages don't need such high resistances, which can drive the noise floor upward. I reduced their 100k and 200k values to 10k and 20k. But that affects the RC time constant of the filter that does the output AC coupling, so I increased the associated capacitor from 100 nF to 1 µF.

## Current draw
15.6 mA +12 V, 15.7 mA -12 V

## Photos

![front](Images/front.jpg)
![front](Images/back.jpg)

## Documentation

* [Build notes](Docs/build.md)
* [Blog post](https://analogoutputblog.wordpress.com/2022/05/17/barton-full-wave-rectifier/)

## Git repository

* [https://gitlab.com/rsholmes/frontpanels/tree/main/Barton_Dual_Rectifiers](https://gitlab.com/rsholmes/frontpanels/tree/main/Barton_Dual_Rectifiers)

## Submodules

This repo uses submodules aoKicad and Kosmo_panel, which provide needed libaries for KiCad. To clone:

```
git clone git@gitlab.com:rsholmes/dualrect.git
git submodule init
git submodule update
```


Alternatively do

```
git clone --recurse-submodules git@gitlab.com:rsholmes/dualrect.git
```

Or if you download the repository as a zip file, you must also click on the "aoKicad" and "Kosmo\_panel" links on the GitHub page (they'll have "@ something" after them) and download them as separate zip files which you can unzip into this repo's aoKicad and Kosmo\_panel directories.

If desired, copy the files from aoKicad and Kosmo\_panel to wherever you prefer (your KiCad user library directory, for instance, if you have one). Then in KiCad, go into Edit Symbols and add symbol libraries 

```
aoKicad/ao_symbols
Kosmo_panel/Kosmo
```
and go into Edit Footprints and add footprint libraries 
```
aoKicad/ao_tht
Kosmo_panel/Kosmo_panel.
```

